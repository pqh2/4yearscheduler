﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _4YearSchedulerWeb.Models
{
    public class ExemptCourse
    {
        public ExemptCourse()
        {
            ;
        }

        public ExemptCourse(string nameToDisplay, bool isExempt)
        {
            m_NameToDisplay = nameToDisplay;
            this.isExempt = isExempt;
        }

        public ExemptCourse(string nameToDisplay)
        {
            isExempt = false;
            m_NameToDisplay = nameToDisplay; 
        }

        public string m_NameToDisplay { get; set; }
        public bool isExempt { get; set; }
    }
}