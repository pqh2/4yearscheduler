﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4YearSchedulerWeb.Models
{
    public class Course
    {
        public Course()
        {
            ;
        }
        public Course(string subject, int Coursenumber, string courseGroupName, List<string> prereqs, int credits)
        {
            IsCourseSet = false;
            m_subject = subject;
            m_courseNumber = Coursenumber;
            m_prerequisites = prereqs;
            m_numberOfCredits = credits;
            m_offeredFallEven = m_offeredFallOdd = m_offeredSpringEven = m_offeredSpringOdd = true;
            CourseGroup = courseGroupName;
        }

        public Course Clone()
        {
            return new Course
            {
                m_subject = (string)m_subject.Clone(),
                m_courseNumber = m_courseNumber,
                m_prerequisites = m_prerequisites.Select(x => ((string)x.Clone())).ToList(),
                m_offeredFallEven = m_offeredFallEven,
                m_offeredFallOdd = m_offeredFallOdd,
                m_offeredSpringEven = m_offeredSpringEven,
                m_offeredSpringOdd = m_offeredSpringOdd,
                m_numberOfCredits = m_numberOfCredits
            };
        }

        public string CourseName
        {
            get { return m_subject + "-" + m_courseNumber.ToString(); }
        }

        public int CourseNumber
        {
            get { return m_courseNumber; }
        }

        public List<string> Prerequisites
        {
            get { return m_prerequisites; }
        }

        public bool OfferedFallEven
        {
            get { return m_offeredFallEven; }
        }

        public bool OfferedFallOdd
        {
            get { return m_offeredFallOdd; }
        }

        public bool OfferedSpringEven
        {
            get { return m_offeredSpringEven; }
        }

        public bool OfferedSpringOdd
        {
            get { return m_offeredSpringOdd; }
        }

        public int NumberOfCredits
        {
            get { return m_numberOfCredits; }
        }
        public string CourseGroup { get; set; }
        public bool IsCourseSet { get; set; }
        string m_subject;
        int m_courseNumber;
        List<string> m_prerequisites;
        bool m_offeredFallEven, m_offeredFallOdd, m_offeredSpringEven, m_offeredSpringOdd;
        int m_numberOfCredits;
    }
}
