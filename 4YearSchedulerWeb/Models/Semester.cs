﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4YearSchedulerWeb.Models
{
    public class Semester
    {
        public Semester()
        {
            creditsInSemester = 0;
            m_courses = new List<Course>();
        }


        public List<Course> m_courses;
        public int creditsInSemester;

        public object Clone()
        {
            return new Semester
            {
                creditsInSemester = creditsInSemester,
                m_courses = m_courses.Select(x => x.Clone()).ToList()
            };
        }
    }
}
