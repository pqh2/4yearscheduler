﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _4YearSchedulerWeb.Models
{
    public class ListOfExemptedCourses
    {
        public ListOfExemptedCourses()
        {
            exemptedCourses = new List<ExemptCourse>();
        }
        public List<ExemptCourse> exemptedCourses { get; set; }
    }
}