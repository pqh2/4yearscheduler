﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4YearSchedulerWeb.Models
{
    class Scheduler
    {
        public Scheduler(List<Course> required, List<Course> exempt, Dictionary<string, int> courseGroups)
        {
            m_possibleCourses = required;
            m_maxCreditPerSemester = 17;
            m_minCreditPerSemester = 8;

            m_routesToNotTake = new Dictionary<int, int>();
            m_coursesTaken = new Dictionary<string, int>();
            m_semesters = new List<Semester>();
            m_semesterLimit = 5;
            m_courseGroups = courseGroups;
            foreach (Course exemptCourse in exempt)
            {
                m_coursesTaken.Add(exemptCourse.CourseName, -1);
                courseGroups[exemptCourse.CourseGroup]--;
            }

        }
        public List<Course> Courses
        {
            get { return m_possibleCourses; }
            private set { ; }
        }

        public int MinCreditPerSemester
        {
            get { return m_minCreditPerSemester; }
            private set { ; }
        }

        public int MaxCreditPerSemester
        {
            get { return m_maxCreditPerSemester; }
            set { ; }
        }


        public List<Semester> Semesters
        {
            get { return m_semesters; }
            set { ; }
        }
        public bool Schedule()
        {
            Semester semester = new Semester();
            m_semesters.Add(semester);
            return InternalSchedule(true, true, semester);
        }
        private bool InternalSchedule(bool EvenYear, bool FallSemester, Semester semester)
        {
            // check if we have a schedule
            if (FoundSchedule())
            {
                return true;
            }

            foreach (Course course in m_possibleCourses)
            {
                // is the course group is not already assigned
                if (!course.IsCourseSet)
                {
                        // is the course offerred and the course has not been taken
                        if (IsCourseOffered(EvenYear, FallSemester, course) && !m_coursesTaken.ContainsKey(course.CourseName) && PrereqsTaken(course))
                        {
                            // we want to add a new semester
                            if (semester.creditsInSemester + course.NumberOfCredits > m_maxCreditPerSemester)
                            {
                                break;
                            }

                            course.IsCourseSet = true;
                            m_coursesTaken.Add(course.CourseName, m_semesters.Count);
                            semester.m_courses.Add(course);
                            semester.creditsInSemester += course.NumberOfCredits;
                            m_courseGroups[course.CourseGroup]--;
                            Semester semesterCopy = semester;
                            int hash = GetHashOfCoursesTaken();

                            // cannot be a set of courses that already caused a failure
                            // or if it is it has to have less credits on the last 
                            // semester than previously (maybe we can fit more in)
                            if (!m_routesToNotTake.ContainsKey(hash) || m_routesToNotTake[hash] > semester.creditsInSemester)
                            {
                                // schedule next course group
                                bool success = InternalSchedule(EvenYear, FallSemester, semester);
                                // if success return true
                                if (success)
                                    return true;
                                // else set route to not take again
                                else
                                    m_routesToNotTake[hash] = semester.creditsInSemester;
                            }

                            // clean up
                            m_courseGroups[course.CourseGroup]++;
                            course.IsCourseSet = false;
                            m_coursesTaken.Remove(course.CourseName);
                            semester = semesterCopy;
                            semester.m_courses.Remove(course);
                            semester.creditsInSemester -= course.NumberOfCredits;

                        }
                }
            }

            // if we none of the courses fit into the current semester, create a new one
            if (TryAddSemester(ref semester) == false) return false;
            else
            {
                return InternalSchedule(EvenYear ^ FallSemester, !FallSemester, semester);

            }

        }

        private bool TryAddSemester(ref Semester semester)
        {

            if (m_semesters.Count == m_semesterLimit || ((m_semesters.Count > 0) && m_semesters.Last().creditsInSemester < m_minCreditPerSemester))
            {
                Semester semesterToDelete = m_semesters.Last();
                foreach (Course course in semesterToDelete.m_courses)
                {
                    course.IsCourseSet = false;
                    m_coursesTaken.Remove(course.CourseName);
                }

                m_semesters.Remove(m_semesters.Last());
                return false;
            }
            else
            {
                m_semesters.Add(new Semester());
                semester = m_semesters.Last();
                return true;
            }
        }

        private bool PrereqsTaken(Course course)
        {
            foreach (string prereq in course.Prerequisites)
            {
                if (!m_coursesTaken.ContainsKey(prereq) || m_coursesTaken[prereq] == m_semesters.Count)
                    return false;
            }
            return true;
        }

        private int GetHashOfCoursesTaken()
        {
            List<string> listOfCoursesTaken = m_coursesTaken.Select(x => x.Key).ToList();
            listOfCoursesTaken.Sort();
            string concatanatedCourseNames = "";
            foreach (KeyValuePair<string,int> courseTaken in m_coursesTaken)
            {
                concatanatedCourseNames += courseTaken.Key + courseTaken.Value.ToString();
            }

            return concatanatedCourseNames.GetHashCode();
        }

        private bool FoundSchedule()
        {            
            foreach (int number in m_courseGroups.Values)
            {
                if (number > 0) return false;
            }
            return true;
        }

        public bool IsCourseOffered(bool EvenYear, bool FallSemester, Course course)
        {
            if ((EvenYear && FallSemester) && course.OfferedFallEven)
                return true;
            else if ((EvenYear && !FallSemester) && course.OfferedSpringEven)
                return true;
            else if ((!EvenYear && FallSemester) && course.OfferedFallOdd)
                return true;
            else if ((!EvenYear && !FallSemester) && course.OfferedSpringOdd)
                return true;
            return false;
        }

        public void PrintSchedule()
        {
            int semesterNumber = 1;
            int sumCred = 0;
            foreach (Semester semester in m_semesters)
            {
                Console.WriteLine("Semester {0}:", semesterNumber++);
                foreach (Course course in semester.m_courses)
                {
                    Console.WriteLine("{0} - {1} credits", course.CourseName, course.NumberOfCredits);
                    sumCred += course.NumberOfCredits;
                }

                Console.WriteLine("Total credits this semester: {0} \n\n", sumCred);
                sumCred = 0;
            }
        }


        /// <summary>
        ///  routes to not take consist of a hash representing the list of courses taken
        ///  and an int representing the number of credits on the last semester for that given set of courses
        /// </summary>
        Dictionary<int, int> m_routesToNotTake;
        Dictionary<string, int> m_coursesTaken;
        Dictionary<string, int> m_courseGroups;
        List<Semester> m_semesters;
        List<Course> m_possibleCourses;
        int m_semesterLimit;
        int m_minCreditPerSemester, m_maxCreditPerSemester;
    }
}
