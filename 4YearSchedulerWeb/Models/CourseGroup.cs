﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _4YearSchedulerWeb.Models
{
    public class CourseGroup
    {

        public CourseGroup Clone()
        {
            return new CourseGroup(m_possibleCourses.Select(x => x.Clone()).ToList());
        }

        public CourseGroup(List<Course> courses)
        {
            courseDecidedUpon = "";
            m_courseSet = false;
            m_possibleCourses = courses;
            m_averageCourseNumber = m_possibleCourses.Average(x => x.CourseNumber);
        }


        public void ChooseASpecificCourse(string courseName)
        {
            m_possibleCourses = m_possibleCourses.Where(x => (x.CourseName == courseName)).ToList();
        }

        public List<Course> PossibleCourses
        {
            get { return m_possibleCourses; }
            private set { ; }
        }

        public bool CourseSet
        {
            get { return m_courseSet; }
            private set { ; }
        }

        public void UnSetCourse()
        {
            m_courseSet = false;
            courseDecidedUpon = "";
        }

        public void SetCourse(string courseName)
        {
            m_courseSet = true;
            courseDecidedUpon = courseName;
        }

        public double AverageCourseLevel
        {
            get { return m_averageCourseNumber; }
        }

        Double m_averageCourseNumber;
        bool m_courseSet;
        string courseDecidedUpon;
        List<Course> m_possibleCourses;
    }
}
