﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _4YearSchedulerWeb.Models
{
    public class ExemptCourseGroup
    {
        public ExemptCourseGroup(CourseGroup courseGroup)
        {
            m_courseGroup = courseGroup;
            isExempt = false;
            m_NameToDisplay = courseGroup.PossibleCourses.Select(x => x.CourseName).Aggregate((x, y) => x + " " + y);
        }

        public string m_NameToDisplay { get; set; }
        public CourseGroup m_courseGroup { get; set; }
        public bool isExempt { get; set; }
    }
}