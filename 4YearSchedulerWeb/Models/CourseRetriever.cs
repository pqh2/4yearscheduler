﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _4YearSchedulerWeb.Models
{
    public class CourseRetriever : _4YearSchedulerWeb.Models.ICourseRetriever
    {
        public CourseRetriever()
        {
            ;
        }
        public Dictionary<string, int> GetCourseGroups(string major)
        {
            if (major.ToUpper() == "CS")
            {
                Dictionary<string, int> courseGroups = new Dictionary<string, int>();
                courseGroups["CS-108"] = 1;
                courseGroups["MATH-156"] = 1;
                courseGroups["MATH-171"] = 1;
                courseGroups["MATH-172"] = 1;
                courseGroups["MATH-271"] = 1;
                courseGroups["CS-112"] = 1;
                courseGroups["CS-212"] = 1;
                courseGroups["CS-214"] = 1;
                courseGroups["CS-232"] = 1;
                courseGroups["CS-262"] = 1;
                courseGroups["ENGR-220"] = 1;
                courseGroups["300Elective"] = 3;
                return courseGroups;
            }
            return new Dictionary<string, int>();
        }

        public List<Course> GetCourses(string major)
        {
            if(major == null)
                return new List<Course>();
            if (major.ToUpper() == "CS")
            {
                Course cs108 = new Course("CS", 108, "CS-108", new List<string>(), 4);
                Course math156 = new Course("MATH", 156, "MATH-156", new List<string>(), 4);
                Course math171 = new Course("MATH", 171, "MATH-171", new List<string>(), 4);
                Course math172 = new Course("MATH", 172, "MATH-172", new List<string>() { "MATH-171" }, 4);
                Course cs112 = new Course("CS", 112, "CS-112", new List<string>() { "CS-108" }, 4);
                Course cs212 = new Course("CS", 212, "CS-212", new List<string>() { "CS-112", "MATH-156" }, 3);
                Course cs344 = new Course("CS", 344, "300Elective", new List<String>() { "CS-212" }, 3);
                Course cs364 = new Course("CS", 364, "300Elective", new List<String>() { "CS-232" }, 3);
                Course cs262 = new Course("CS", 262, "CS-262", new List<string>() { "CS-112" }, 3);
                Course engr220 = new Course("ENGR", 220, "ENGR-220", new List<string> { "CS-108" }, 4);
                Course cs332 = new Course("CS", 332, "300Elective", new List<String>() { "CS-232" }, 3);
                Course cs232 = new Course("CS", 232, "CS-232", new List<String>() { "CS-112", "ENGR-220" }, 3);
                Course cs214 = new Course("CS", 214, "CS-214", new List<string>() { "CS-108" }, 3);
                Course cs372 = new Course("CS", 372, "300Elective", new List<string>() { "CS-108" }, 3);
                Course math271 = new Course("MATH", 271, "MATH-271", new List<string>() { "MATH-172" }, 4);

                             List<Course> courses = new List<Course>() { cs108, cs112, cs212, cs232, cs214, cs262, cs332, cs344, cs364, cs372, math156, math171, math172, math271, engr220 };
 
                courses.Sort((x, y) => CompareByCourseNumber(x, y));
                return courses;
            }
             return new List<Course>();
        }

        private static int CompareByCourseNumber(Course x, Course y)
        {
            return x.CourseNumber.CompareTo(y.CourseNumber);
        }
    }
}
