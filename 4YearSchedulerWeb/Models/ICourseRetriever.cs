﻿using System;
namespace _4YearSchedulerWeb.Models
{
    public interface ICourseRetriever
    {
        System.Collections.Generic.Dictionary<string, int> GetCourseGroups(string major);
        System.Collections.Generic.List<Course> GetCourses(string major);
    }
}
