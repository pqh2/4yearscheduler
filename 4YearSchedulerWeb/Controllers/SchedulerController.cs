﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _4YearSchedulerWeb.Models;

namespace _4YearSchedulerWeb.Controllers
{
    public class SchedulerController : Controller
    {
        private Scheduler scheduler;
        private List<Course> courses;
        private Dictionary<string, int> courseGroups;
        private ICourseRetriever m_retriever;

        public SchedulerController(ICourseRetriever retriever)
        {
            m_retriever = retriever;
        }
        //
        // GET: /Scheduler/

        public ActionResult Schedule()
        {
            courses = m_retriever.GetCourses("CS");
            courseGroups = m_retriever.GetCourseGroups("CS");
            scheduler = new Scheduler(courses, new List<Course>(), courseGroups);
            scheduler.Schedule();
            return View(scheduler.Semesters);
        }
        [HttpPost]
        public ActionResult Schedule(ListOfExemptedCourses exempt)
        {

            courses = m_retriever.GetCourses("CS");
            Dictionary<string, bool> dictionaryOfExempt = new Dictionary<string, bool>();
            foreach (ExemptCourse exemptCourse in exempt.exemptedCourses)
            {
                if(!exemptCourse.isExempt)
                    dictionaryOfExempt.Add(exemptCourse.m_NameToDisplay, exemptCourse.isExempt);
            }

            courseGroups = m_retriever.GetCourseGroups("CS");
            scheduler = new Scheduler(
                courses
                .Where(x => dictionaryOfExempt.ContainsKey(x.CourseName))
                .Select(y => y)
                .ToList(),
                courses
                .Where(x => !dictionaryOfExempt.ContainsKey(x.CourseName))
                .Select(y => y)
                .ToList(),
                courseGroups
                );
            scheduler.Schedule();
            return View(scheduler.Semesters);
        }


        public ActionResult MajorPick()
        {
            return View(new MajorModel { name = "default" });
        }

        [HttpPost]
        public ActionResult MajorPick(MajorModel major )
        {
            return View(new MajorModel { name = major.name });
        }

        [ChildActionOnly]
        public ActionResult GetExemptCoursesForMajor(MajorModel major)
        {
            courses = m_retriever.GetCourses(major.name);
           
            ListOfExemptedCourses listOfExempted = new ListOfExemptedCourses();
            foreach (Course course in courses)
            {

                listOfExempted.exemptedCourses.Add(new ExemptCourse
                {
                    isExempt = false,
                    m_NameToDisplay = course.CourseName
                });
            }
            return View(listOfExempted);
        }

    }
}
